                                                              -*- org -*-
#+TITLE: sequoia-sqv NEWS – history of user-visible changes
#+STARTUP: content hidestars

* Changes in 1.3

- This release adds support for RFC 9580.
- sqv --policy-as-of can be used to use the policy at a different
  time.
- sqv now supports verification of detached signatures using the
  `--signature-file` argument, inline-signed messages using the
  `--message` argument, and messages using the Cleartext Signature
  Framework using the `--cleartext` argument.  When verifying
  inline-signed messages and cleartext messages, `--output` is
  mandatory.  This closely resembles `sq verify`.  The legacy
  two-argument form is supported for the duration of the 1.x series.

* Changes in 1.2.1

- Use the correct stem when saving shell completions.

* Changes in 1.2.0

- Upgrade to clap 4.
- shell completions are now written to
  `$ASSET_OUT_DIR/out/shell-completions`.
- Generate a man page.  It is written to `$ASSET_OUT_DIR/out/man-pages`.
- Use the system's crypto policy by default.  Allow the user to
  override it by setting `SEQUOIA_CRYPTO_POLICY`.

* Changes in 1.1.0

  This is the first release since sqv was moved to its own repository.
  All dependencies have been upgraded.  No code was changed.

* Changes in 1.0.0

This is the initial stable release.
